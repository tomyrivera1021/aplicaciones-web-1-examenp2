// peticion de envio de registro de usuario
router.post("/registro-usuarios", (req, res) => {
    //   console.log(req.body);
    const { cedula, apellido, nombre, direccion, telefono, email } = req.body;
    //  declaramos una variable para almacenar la contraseña encriptada
    conexion.query(
    "insert into USUARIOS  SET ?",
        {
        USUARIOS_CEDULA: cedula,
        USUARIOS_APELLIDO: apellido,
        USUARIOS_NOMBRE: nombre,
        USUARIOS_DIRECCION: direccion,
        USUARIOS_TELEFONO: telefono,
        USUARIOS_EMAIL: email,    
        },
    (error, results) => {
        if (error) {
            console.log("fallo el registro");
            console.log(error);
        } else {
            console.log("Registro almacenado en la base de datos");
            res.redirect("index.html");
        }
        }
    );
});