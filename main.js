console.log("Examen P2");
// declaracion de variables
const inputs = document.querySelectorAll("form input"),
    adver = document.getElementById("warning");
// declaracion de variable para guardar el valor de la contraseÃ±a
var pass;
// declaracion de las expresiones regulares para la validacion
const expresionesRegulares = {
    numeros: /([0-9])/,
    text: /([a-zA-Z])/,
    caracteres: /[^a-zA-Z\d\s]/,
    correo:
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
    espacios: /\s/g,
};

// instruccion que valida la forma de ingresar los datos del usuario
inputs.forEach((input) => {
    input.addEventListener("keyup", (e) => {
        let valueInput = e.target.value;
        let style = e.target.style;

    switch (e.target.id) {
        case "cedula":
            input.value = valueInput
                .replace(expresionesRegulares.caracteres, "")
                .replace(expresionesRegulares.espacios, "")
                .replace(expresionesRegulares.text, "");
            if (expresionesRegulares.numeros.test(valueInput)) {
                if (valueInput.length < 10) {
                message("Debe ingresar los 10 numeros de su CI");
                style.border = "2px solid #ce1212";
                } else {
                message("Numero de CI aceptable");
                style.border = "2px solid #008f39";
            }
            } else {
                message(
                "Debes ingresar su numero de cedula sin signos especiales ni letras"
                );
                style.border = "2px solid #ce1212";
            }
            break;

        case "nombre":
        message("Debe ingresar solo letras");
            input.value = valueInput
                .replace(expresionesRegulares.caracteres, "")
                .replace(expresionesRegulares.numeros, "");
        if (valueInput.length == 0) {
            style.border = "2px solid #ce1212";
        } else {
            style.border = "2px solid #008f39";
        }
        break;

        case "apellido":
        message("Debe ingresar solo letras");
            input.value = valueInput
                .replace(expresionesRegulares.caracteres, "")
                .replace(expresionesRegulares.numeros, "");
        if (valueInput.length == 0) {
            style.border = "2px solid #ce1212";
        } else {
            style.border = "2px solid #008f39";
        }
        break;

        case "email":
        input.value = valueInput.replace(expresionesRegulares.espacios, "");
        if (expresionesRegulares.correo.test(valueInput)) {
            style.border = " 2px solid #008f39";
            message("Correo electronico correcto");
        } else {
            style.border = "2px solid #ce1212";
            message("El correo que ingreso no es correcto");
        }
        break;

        case "telefono":
        input.value = valueInput
            .replace(expresionesRegulares.caracteres, "")
            .replace(expresionesRegulares.espacios, "")
            .replace(expresionesRegulares.text, "");
        if (expresionesRegulares.numeros.test(valueInput)) {
            if (valueInput.length < 10) {
            message("Debe ingresar los 10 numeros de su telefono");
            style.border = "2px solid #ce1212";
        } else {
            message("Numero de telefono aceptable");
            style.border = "2px solid #008f39";
        }
        } else {
            message(
            "Debes ingresar solo numeros sin signos especiales ni letras"
            );
            style.border = "2px solid #ce1212";
        }
        break;

    }
    });
});
// funcion que permite el registro de un nuevo usuario
function register() {
  // funcion para crear objetos de tipo cliente
function Client(cedula, apellido, nombre, direccion, telefono, email) {
    //   atributos del objeto cliente
    this.cedula = cedula;
    this.apellido = apellido;
    this.nombre = nombre;
    this.direccion = direccion;
    this.telefono = telefono;
    this.email = email;
  }
  // declaramos un array para guardar temporalmente los datos de los usuarios
  let dataBase = [];
  //   se capturan los valores ingresados por el usuario en los input
    let cedulaInput = document.getElementById("cedula").value;
    let apellidoInput = document.getElementById("apellido").value;
    let nombreInput = document.getElementById("nombre").value;
    let direccionInput = document.getElementById("direccion").value;
    let telefonoInput = document.getElementById("telefono").value;
    let emailInput = document.getElementById("email").value;
  // comprueba si algun input obligatorio esta vacio
if (
    blank(cedulaInput) ||
    blank(apellidoInput) ||
    blank(nombreInput) ||
    blank(direccionInput) ||
    blank(telefonoInput) ||
    blank(emailInput)
) {
    alert("Llene todos los campos obligatorios para realizar su registro");
} else {
    //  instanciamos un nuevo objeto y llamamos a la funcion que crea el objeto, le pasamos como parametros las variables donde se guardaron los valores ingresados
    let newClient = new Client(
        cedulaInput,
        apellidoInput,
        nombreInput,
        direccionInput,
        telefonoInput,
        emailInput
    );
    // agregamos nuestro objeto al array donde se guardara temporalmente
    dataBase.push(newClient);
    console.log(dataBase);
  }
}

// funcion que comprueba si un input esta vacio
function blank(par) {
    if (par === "") {
        return true;
    } else {
        return false;
    }
}

// funcion que envia un mensaje de informacion al usuario
function message(val) {
    adver.innerHTML = val;
}
